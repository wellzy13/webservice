$(document).ready(function () {
    $("#returnid").click(function () {
        var value = $("#batchnumber").val();
        // alert(value);
        getRegPlates(value);
    })
});

/**
 * Get the reg plates which are contained within a specific ID. Example batch 10.
 * Loop through each element of the price element of the array and add it to the previous value
 * to get the overall for the batch of 10 plates.
 * the price is then passed through the update high bid method to ensure that the minimum bid for
 * the specific batch cannot be less than the price.
 * @param batch passing batch id through the method.
 */
function getRegPlates(batch) {
    if (batch == 0 || isNaN(batch)) {
        window.alert("Empty batch");

    } else {
        $.ajax({
            type: "GET",
            url: "/batch/" + batch + "/regplates",
            contentType: "application/json",
            dataType: "json"
        }).then(function (data) {
            var batch = data._embedded.regplate;
            var price = 0;

            for (var i = 0; i < batch.length; i++) {
                $('#reg'+(i)).html(data._embedded.regplate[i].licenceplate);
                price += data._embedded.regplate[i].price;

            }
            updateHighBid(price);
        });
    }

}

/**
 * 'temp price takes the value of the current bid in the database, however, that bid can be null
 * if no business has bid on that batch so it must be checked. The current bid will be minimum 200
 * from th eprice of the reg plates so temp price becomes that value to check the following bids against.
 * @param price passed through the method to check the current bid against.
 */
function updateHighBid(price){
    var batchid = $('#batchnumber')[0].value;
    console.log("BATCH: " + batchid);

    $.ajax({
        type: "GET",
        url: "/batch/" + batchid,
        contentType: "application/json",
        dataType: "json"
    }).then(function (data) {

        var tempPrice = data.bid;
        console.log("TEMPRICE" + tempPrice);
        if (tempPrice > price){
            $('#currentbid')[0].value = tempPrice;
            console.log("tem" + tempPrice);
        }
        else {
            $('#currentbid')[0].value = price;
        }
    });
}

/**
 * method to check the current highest batch id in the database so users dont select a higher value than
 * what is attainable.
 */
// function checkhighid() {
//     var id ="";
//     $.ajax({
//         type: "GET",
//         url: "/batch/" ,
//         contentType: "application/json",
//         dataType: "json"
//     }).then(function (data){
//        id = data.page.totalElements;
//         $("#insertno").html('<input type="number" id="batchnumber" min="0" max="'+id+'" placeholder="Enter Batch No">')
//
//
//     });
//     return id;
// }

/**
 * if the bid entered is more than the current bid (always 200 or more) then the current high bid changes
 * to the bid which was enetered and calls the updatebid method which stores the business id and bid etc in the
 * database. If the bid is not higher then a message is displayed to the user.
 */
function checkBid() {
    if ($('#enterbid')[0].value > $('#currentbid')[0].value){
        $('#currentbid')[0].value = $('#enterbid')[0].value;
        updateBid();
    }
    else {
        alert("Price must be higher than " + $('#currentbid')[0].value);
    }

}

/**
 * get the business id, batch id and bid of the batch and store them in database if the check bid function
 * is successful.
 */
function updateBid() {

    var businessid = $('#dropdownbusinesses')[0].value;
    var batchid = $('#batchnumber')[0].value;
    var currentbid = $('#enterbid')[0].value;

    $.ajax({
        type: "PATCH",
        url: "/batch/" + batchid,
        data: '{"businessid":"/business/'+businessid+'", "bid":"'+currentbid+'"}',
        contentType: "application/json",
        dataType: "json"
    })
}

// function RegisterUser() {
//
//     var businessaddress = $('#businessaddress')[0].value;
//     var contactnumber = $('#number')[0].value;
//     var businessname = $('#businessname')[0].value;
//     var email = $('#email')[0].value;
//     $.ajax({
//         type: "POST",
//         url: "/business/",
//         data: '{"businessid":"/business/'+businessname+'", "contactnumber":"'+contactnumber+'",' +
//             '' + '"email":"'+email+'", ' + ' "businessaddress":"'+businessaddress+'"}',
//         contentType: "application/json",
//         dataType: "json"
//     })
// }