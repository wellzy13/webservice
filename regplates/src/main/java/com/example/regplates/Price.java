package com.example.regplates;


public class Price extends RegPlatesGen{
    /**
     *
     * @param letters will be the last 3 characters of the licence plate passed through
     *               this method.
     * @return price with a multiplication if the last 3 characters contain any duplicate
     * characters.
     * else return the normal value of a licence plate. (the return price will be limited
     * to 2 decimal places
     * e.g 31.05).
     * If the firs character of the third octet is equal to any other character then the
     * licence plate
     * is deemed to be more valuable so it will generate a higher price than usual.
     */
    double priceGen(String letters){
        double price = 20.00;
        char[] reg = letters.toCharArray();

        if (reg[4] == reg[5] || reg[4] == reg[6]){
            double temp = price * ((Math.random() * ((10 - 1) + 1)) + 1);
            return Math.floor(temp * 100) / 100; // Ensure the decimal place
            // is set at 2 decimal places.
        }
        return price;
    }
}
