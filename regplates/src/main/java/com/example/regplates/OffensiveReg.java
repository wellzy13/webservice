package com.example.regplates;

class OffensiveReg extends RegPlatesGen {

    /**
     * Array of words deemed to be offensive for when the reg plate is generated
     * it can check against
     * this array for any offensive words.
     */
    private static String[] OFFENSIVE_PLATE_LETTERS = {"FUC", "TWA", "CUN",
            "DIC", "ABC", "TIT", "NAZ", "COC", "DUM", "FAN", "NOB", "FAT",
            "DIK", "CON", "BAS", "CUM", "XXX", "ZZZ", "FUk", "KKK", "SEX"};


    /**
     *
     * @param letters would typically be the region of the reg plate
     *               (in this case randomaly generated letters.
     * @param digits this is the series of the reg plate (19).
     * @param letters2 randomaly generated letters for the last 3
     *                 characters of the reg plate, the offensive
     * criteria will be checked against this.
     * @return true if an offensive reg plate has been detected
     */
    static boolean illegalWord(String letters, String digits, String letters2) {
        for (int i = 0; i < OFFENSIVE_PLATE_LETTERS.length; i++) {
            if (letters2.equals(OFFENSIVE_PLATE_LETTERS[i])) {
                System.out.println("THIS IS AN OFFENSIVE REG PLATE: >  "
                        + letters + digits + "-" + letters2);
                System.out.println("New reg plate = "
                        + letters + digits + "-" + letters2.replace(letters2,
                        generateRandomChars()));
                return true;
            }
        }
        return false;
    }


}
