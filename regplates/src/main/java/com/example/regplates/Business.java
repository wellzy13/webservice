package com.example.regplates;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
@Entity
public class Business implements java.io.Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "businessid")
    private int businessid;

    /**
     * Field businessname cannot be null as an id must have a name to coincide with it.
     */

    @NotNull
    @Size(min = 1, max = 20)
    private String businessname;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "businessid")
    private List<Batch> batches;

    /**
     * Default constructor for business class
     */
    Business(){

    }

    /**
     * GETTERS and SETTERS for the Business class.
     * @return the relevant data based on the fields in the database and class.
     */
    public int getBusinessid() {
        return businessid;
    }

    public void setBusinessid(int businessid) {
        this.businessid = businessid;
    }

    public List<Batch> getBatches() {
        return batches;
    }

    public void setBatches(List<Batch> batches) {
        this.batches = batches;
    }

    public String getBusinessname() {
        return businessname;
    }

    public void setBusinessname(String businessname) {
        this.businessname = businessname;
    }
}
