package com.example.regplates;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "batch", path = "batch")
public interface BatchRepository extends PagingAndSortingRepository<Batch, Integer> {


}

