package com.example.regplates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegplatesApplication {

    public static void main(String[] args) {
        SpringApplication.run(RegplatesApplication.class, args);
    }

}
