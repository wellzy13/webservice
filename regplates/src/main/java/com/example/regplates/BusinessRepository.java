package com.example.regplates;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "business", path = "business")
public interface BusinessRepository extends PagingAndSortingRepository<Business, Integer> {


}
