package com.example.regplates;


import static com.example.regplates.OffensiveReg.illegalWord;

class RegPlatesGen {

    // SERIES ALWAYS 19 TO ONLY CREATE COMMON REG PLATES
    private static final int SERIES = 19;



    /**
     * Generate 2 random letters [random][random][series][series]..
     * The first two characters would typically be the region of the sale of the car ..
     * In this case randomly generate.
     * @return two random characters and append them to create a string.
     */
    private static String generateRegion() {
        StringBuilder lettersOne = new StringBuilder();
        int n = 'Z' - 'A' + 1;
        for (int i = 0; i < 2; i++) {
            char c = (char) ('A' + Math.random() * n);
            lettersOne.append(c);
        }
        return lettersOne.toString();
    }

    /**
     * Similar to the generate region method - generate 3 random characters..
     * For the end of the reg plate so the reg plate would appear as follows:
     * [random][random][series][series]-[random][random][random].
     * @return the 3 random characters and append them to create a string.
     */
    static String generateRandomChars() {
        StringBuilder newLetters = new StringBuilder();
        int n = 'Z' - 'A' + 1;
        for (int i = 0; i < 3; i++) {
            char c = (char) ('A' + Math.random() * n);
            newLetters.append(c);
        }
        return newLetters.toString();
    }

    /**
     * Method to generate licence plates utilising the methods (generateRegion(), generateRandomChars() &
     * fixedSeries())..
     * The last 3 random chars are checked against the offensive criteria in the OffensiveReg class.
     * @return licence plate which is not offensive.
     */

    String generateLicensePlate() {
        String licensePlate, letters2;
        String letters = generateRegion();
        String digits = String.valueOf(SERIES);
        do {
            letters2 = generateRandomChars();
        } while (illegalWord(letters, digits, letters2));

        licensePlate = letters + digits + letters2;

        return licensePlate;

    }









}
