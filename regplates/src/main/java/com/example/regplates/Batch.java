package com.example.regplates;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Batch implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int batchid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "businessid")
    private Business businessid;

    private Double bid;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "batchid")
    private List<Regplate> regplates;



    public int getBatchid() {
        return batchid;
    }

    public void setBatchid(int batchid) {
        this.batchid = batchid;
    }

    public Business getBusinessid() {
        return businessid;
    }

    public void setBusinessid(Business businessid) {
        this.businessid = businessid;
    }

    public List<Regplate> getRegplates() {
        return regplates;
    }

    public void setRegplates(List<Regplate> regplates) {
        this.regplates = regplates;
    }

    public Double getBid() {
        return bid;
    }

    public void setBid(Double bid) {
        this.bid = bid;
    }
}
