package com.example.regplates;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "regplate", path = "regplate")
public interface RegplateRepository extends PagingAndSortingRepository<Regplate, Integer> {
}
