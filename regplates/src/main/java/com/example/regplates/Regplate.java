package com.example.regplates;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * Licence plate class which will hold data regarding licence plates.
 */

@Entity
public class Regplate implements java.io.Serializable{

    /**
     * primary key from the licence table in postgres
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int licenceid;

    /**
     * input validation, generally some constraints which are enforced on a database.
     */

    @NotNull
    @Size(min = 7, max = 7)
    private String licenceplate;
    private double price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "batchid")
    private Batch batchid;

    /**
     * default constructor
     */

    public Regplate(){

    }

    /**
     * constructor to receive each attribute
     */

    public Regplate(String licenceplate, double price){
        this.licenceplate = licenceplate;
        this.price = price;
    }

    public int getLicenceid() {
        return licenceid;
    }

    public void setLicenceid(int licenceid) {
        this.licenceid = licenceid;
    }

    public String getLicenceplate() {
        return licenceplate;
    }

    public void setLicenceplate(String licenceplate) {
        this.licenceplate = licenceplate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Batch getBatchid() {
        return batchid;
    }

    public void setBatchid(Batch batchid) {
        this.batchid = batchid;
    }
}
