package com.example.regplates;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class RegplateDatabaseInsert {

    RegplateRepository regplateRepository;
    BatchRepository batchRepository;

    /**
     *
     * @param regplateRepository passed through the constructor.
     * @param batchRepository passed through the constructor.
     * the constrcutor when usec (when application runs) will by default use the insert method
     * to send information to the database.
     */
    RegplateDatabaseInsert(RegplateRepository regplateRepository, BatchRepository batchRepository){
        this.regplateRepository = regplateRepository;
        this.batchRepository = batchRepository;
        insert();
    }

    /**
     * Inserting data into the database when the applications runs. Will generate a batch of 10 reg plates
     * with the same batch Id. Each reg plate will be passed through the priceGen method to determine a value
     * for that plate.
     * Try and save the reg plate to the database and if there is a duplicate (highly unlikely) print the
     * error stack trace.
     */
    @Transactional
    /**
     * Each time the application is run it will generate a batch of 10 regplates and save the plates to the
     * database via the reg plate repository.
     */
    void insert(){

        Batch batch = new Batch();
        batchRepository.save(batch);


        for (int i = 0; i < 10; i++) {
            Regplate regplate = new Regplate();
            regplate.setBatchid(batch);
            RegPlatesGen gen = new RegPlatesGen();
            String temp = gen.generateLicensePlate();
            regplate.setLicenceplate(temp);
            regplate.setPrice(new Price().priceGen(temp));
            try {
                regplateRepository.save(regplate);
            }catch (ConstraintViolationException c){
                c.printStackTrace();
            }

        }

//        /**
//         * Testing to check if the stack trace worked if a duplicate entry was attempted in the database
//         *  >>> WORKED
//         */
//        try {
//            Regplate regplate = new Regplate();
//            regplate.setLicenceplate("CL19ZWW");
//            regplateRepository.save(regplate);
//        }catch (Exception e){
//            e.printStackTrace();
//        }



    }


}
